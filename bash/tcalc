#!/bin/sh


# Skel variables
script_name="$(basename $0)"
initial_folder="$(pwd)"

# Default optional options values
verbose="no"

# Required parameters
# set max to any negative number for an arbitrary default parameter count
default_parameter_max=0
default_parameter_min=0
default_parameters=()

# TODO Options from config file
# TODO Log file options -- Dar uma olhada no commando logger, só não encontrei onde ele dá saída (além do stderr)
# TODO If a option is defined more than one time should stop

# usage [ exit_code [ error_message ] ] 
function usage() {
    if [ $# -eq 2 ]; then
        echo
        echo "Error: $2"
    fi

    echo
    echo "$script_name [ -of | --optional_flag_long_form ] [ -po | --parametrized_option <sub folder name>] [ -v | --verbose ] [ -h | --help ] default_parameter another_default_parameter ..."
    echo
    echo " -e, --entrada   Hora de entrada (formato: \"HH:MM\")"
    echo " -s, --saida     Hora de saída (formato: \"HH:MM\")"
    echo " -h, --help                       This help message"
    echo

    # TODO Examples
    # TODO Return values

    if [ $# -gt 0 ]; then
        exit $1
    fi
}

function parse_command_line() {
    local default_parameters_found=0

    while [ $# -gt 0 ]; do
        case "$1" in
            "-e" | "--entrada")
                entrada_str="$2"
                shift 2
            ;;

            "-s" | "--saida")
                saida_str="$2"
                shift 2
            ;;

            "-v" | "--verbose")
                verbose="yes"
                shift 1
            ;;

            "-h" | "--help")
                usage 0
            ;;

            *)
                default_parameters_found=$((default_parameters_found + 1))

                if [ $default_parameter_max -gt -1 ]; then
                    if [ "${#default_parameters[@]}" -gt $default_parameter_max ]; then
                        usage 1 "$default_parameter_max parameters at most can be used...."
                    fi
                fi

                default_parameters=("${default_parameters[@]}" "$1")
                shift 1
            ;;
        esac
    done

     # or is -o
     # ( $default_parameter_max >   -1 and "${#default_parameters[@]}" >   $default_parameter_max )
    if [ $default_parameter_max -gt -1 -a  "${#default_parameters[@]}" -gt $default_parameter_max ]; then
        usage 1 "Up to $default_parameter_max parameters can be informed (${#default_parameters[@]} found)"
    fi

    if [ "${#default_parameters[@]}" -lt $default_parameter_min ]; then
        usage 1 "At least $default_parameter_min parameters must be informed"
    fi
}

function check_dependencies_availability() {
    for dependency in "${dependencies[@]}"; do
        which "$dependency" &> /dev/null || usage 99 "$dependency command must be available to this script work properly, check your distro packages"
    done
}

function main() {
    parse_command_line "$@"

    check_dependencies_availability

    entrada_hora=$(echo $entrada_str | awk '{ print(substr($0, 1, index($0, ":")-1)); }' )
    [ $verbose == "yes" ] && echo "entrada_hora: $entrada_hora"
    entrada_minuto=$(echo $entrada_str | awk '{ print(substr($0, index($0, ":")+1, length($0))); }' )
    [ $verbose == "yes" ] && echo "entrada_minuto: $entrada_minuto"

    entrada_minutos=$(( (entrada_hora * 60) + entrada_minuto ))
    [ $verbose == "yes" ] && echo "entrada_minutos: $entrada_minutos"

    saida_hora=$(echo $saida_str | awk '{ print(substr($0, 1, index($0, ":")-1)); }' )
    [ $verbose == "yes" ] && echo "saida_hora: $saida_hora"
    saida_minuto=$(echo $saida_str | awk '{ print(substr($0, index($0, ":")+1, length($0))); }' )
    [ $verbose == "yes" ] && echo "saida_minuto: $saida_minuto"
    saida_minutos=$(( (saida_hora * 60) + saida_minuto ))
    [ $verbose == "yes" ] && echo "saida_minutos: $saida_minutos"

    total_minutos=$(( saida_minutos - entrada_minutos ))
    [ $verbose == "yes" ] && echo "total_minutos: $total_minutos"

    echo "$(( total_minutos / 60 ))h + $(( total_minutos % 60 ))m  [$total_minutos minutos]"
}


main "$@"

cd "$initial_folder"
