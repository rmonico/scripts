#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import datetime
import calendar


class FullCalendar(object):

    def __init__(self, birth, life_expectancy):
        self.birth = birth
        self.life_expectancy = life_expectancy

    def print_headers(self):
        print("Age;;Calendar;;;Week day;;;;;;;;")
        sub_header = "Weeks;Years;Week;Month;Year;"

        for delta in range(7):
            sub_header += calendar.day_name[(self.birth + datetime.timedelta(delta)).weekday()] + ";"

        print(sub_header)

    def print_week(self, week_num):
        first_day = self.birth + datetime.timedelta(week_num * 7)
        week_str = str(week_num) + ";" + str(week_num // 52) + ";" + first_day.strftime("%W;%B;%Y;%d;")

        for delta in range(1, 7):
            week_str += (first_day + datetime.timedelta(delta)).strftime("%d;")

        print(week_str)

    def print(self):
        self.print_headers()

        for week in range(0, self.life_expectancy * 52):
            self.print_week(week)


class AgeCalendar(object):

    def __init__(self, birth, life_expectancy):
        self.birth = birth
        self.life_expectancy = life_expectancy

    def print(self):
        for year in range(self.life_expectancy):
            print(self.make_year_str(year))

    def make_year_str(self, year):
        year_str = str(year) + "\t"

        for week in range(1, 52):
            year_str += self.week_status_char(self.get_week_status(year, week))

        return year_str

    def week_status_char(self, status):
        if status == "past":
            return "\u2612"
        elif status == "current":
            return "\u26f6"
        else:
            return "\u2610"

    def get_week_status(self, year, week):
        week_start = self.birth + datetime.timedelta(weeks=(year * 52) + week)
        week_end = week_start + datetime.timedelta(6)

        today = datetime.datetime.today()

        if week_end < today:
            return "past"
        elif week_start > today:
            return "future"
        else:
            return "current"


def parse_command_line():
    parser = argparse.ArgumentParser()

    parser.add_argument("-k", "--calendar-kind", required=True, choices=["full", "age-based"], help="Possible calendar types")
    parser.add_argument("-b", "--birth", required=True, type=lambda s: datetime.datetime.strptime(s, '%d/%m/%Y'), help="Birth date to generate calendar for. Format: dd/mm/yyyy")
    parser.add_argument("-l", "--life-expectancy", required=True, type=int, help="Life life expectancy in years")

    return parser.parse_args()


def main():
    args = parse_command_line()

    if args.calendar_kind == "full":
        calendar = FullCalendar(args.birth, args.life_expectancy)
    elif args.calendar_kind == "age-based":
        calendar = AgeCalendar(args.birth, args.life_expectancy)

    calendar.print()


if __name__ == '__main__':
    main()
