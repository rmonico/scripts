#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import unittest
from taskwarriorreportparser import TaskwarriorReportParser


class TaskwarriorReportWrapperTests(unittest.TestCase):

    def test_ID_column_location(self):
        parser = TaskwarriorReportParser()

        parser.set_header_line("Deps ID  Descrição")

        self.assertEqual(parser._id_column_start, 5)
        self.assertEqual(parser._id_column_end, 8)

    def test_ID_column_parsing(self):
        parser = TaskwarriorReportParser()

        parser.set_header_line("Deps ID  Descrição")

        id = parser.getId("     272 Ver a resposta do pessoal do hashcat (acho que tem email)")

        self.assertEqual(id, 272)


    def test_thin_ID_column_parsing(self):
        parser = TaskwarriorReportParser()

        parser.set_header_line("Deps ID  Descrição")

        id = parser.getId("       2 Ver a resposta do pessoal do hashcat (acho que tem email)")

        self.assertEqual(id, 2)


if __name__ == '__main__':
    unittest.main()
