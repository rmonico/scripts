#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
from sqllinter import SQLLinter

class TestSQLLinter(unittest.TestCase):

    def test_with_single_query(self):
        linter = SQLLinter('select * from table')

        linter.lint()


        self.assertEqual(linter.output[0], 'select')
        self.assertEqual(linter.output[1], '  *')
        self.assertEqual(linter.output[2], 'from')
        self.assertEqual(linter.output[3], '  table')
        self.assertEqual(len(linter.output), 4)

if __name__ == '__main__':
    unittest.main()
