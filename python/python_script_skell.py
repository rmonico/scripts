#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse


def parse_command_line():
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--str-argument")
    parser.add_argument("-f", "--flag", action="store_true")
    parser.add_argument("-d", "--date", type=lambda s: datetime.datetime.strptime(s, '%Y-%m-%d'))
    parser.add_argument("-fp", "--file-parameter", type=argparse.FileType('r'), help="A file that should exist on file system when script is ran")
    parser.add_argument("remainder", nargs=argparse.REMAINDER)

    return parser.parse_args()


def main():
    args = parse_command_line()

    print(args)

if __name__ == '__main__':
    main()
