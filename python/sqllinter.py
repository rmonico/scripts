#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse


class SQLLinter(object):
	
	def __init__(self, sql):
		self.sql = sql

	def lint(self):
		self.output = ['select', '  *', 'from', '  table']


def parse_command_line():
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--str-argument")
    parser.add_argument("-f", "--flag", action="store_true")
    parser.add_argument("remainder", nargs=argparse.REMAINDER)

    return parser.parse_args()


def main():
    args = parse_command_line()

    print(args)

if __name__ == '__main__':
    main()
