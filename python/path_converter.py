#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import re


def parse_command_line():
    parser = argparse.ArgumentParser()

    parser.add_argument("paths", nargs=argparse.REMAINDER, help = "Paths to be converted, different path types can be mixed")

    return parser.parse_args()


def detect_path_kind(path):
    match = re.search('^/[A-Z]/.*', path)

    if match:
        return "cygwin"

    match = re.search('^[a-zA-Z]:\\\\.+', path)

    if match:
        return "windows"
    else:
        return "unknown"


def convert_to_cygwin_path(windows_path):
    print("TODO: convert_to_cygwin_path")


def convert_to_windows_path(cygwin_path):
    print("TODO: convert_to_windows_path")


def main():
    args = parse_command_line()

    for path in args.paths:
        path_kind = detect_path_kind(path)

        if path_kind == "windows":
            converted_path = convert_to_cygwin_path(args.paths)
        elif path_kind == "cygwin":
            converted_path = convert_to_windows_path(args.paths)

        if path_kind != "unknown":
            print("{} => {}".format(path, converted_path))
        else:
            print("Unknown path kind: \"{}\"".format(path))

    if not args.paths:
    	print("No paths found")

if __name__ == '__main__':
    main()
