#!/bin/python3

import sys

__DEBUGGING__ = False


def _log(format, params):
    if __DEBUGGING__:
        print(format.format(params))


def help(args):
    """Mostra esta ajuda"""

    print()
    print("Help")
    print()

    functions = []

    for key in globals():
        obj = globals()[key]
        if obj and hasattr(obj, "__call__") and not obj.__name__.startswith("_"):
            functions.append(obj)

    widest_function_name = 0

    for function in functions:
        if len(function.__name__) > widest_function_name:
            widest_function_name = len(function.__name__)

    format_string = "{:" + str(widest_function_name) + "}   : {}"

    for function in functions:
        print(format_string.format(function.__name__, function.__doc__))

    print(format_string.format("off", "Desregistrar todas as funções criadas pelo .autorun"))
    print(format_string.format("reload", "Recarregar o .autorun"))

    print()


def a_function(args):
    """a_function help text, is shown on help command, function return code (must be a integer) is returned as script return code"""
    return _grep(args)


def _command_not_found():
    _printerr("[functions.py] Command not found: {}\n".format(sys.argv[1]))
    return 127


def _main():
    if len(sys.argv) > 1:
        function = globals().get(sys.argv[1])

        if function:
            return function(sys.argv[2:])
        else:
            return _command_not_found()
    else:
        return _command_not_found()

if __name__ == '__main__':
    sys.exit(_main())
