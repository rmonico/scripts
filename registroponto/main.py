#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import datetime
import os


def parse_command_line():
    parser = argparse.ArgumentParser(prog="ponto",
                                     epilog="Times must be in format 'hh:mm', for example: '19:08'\n" + "Look for registry file/folder in ")

    parser.add_argument("-v", "--verbose", action="store_true",
                        help="Enable verbosity")
    parser.add_argument(
        "-r", "--registry", help="Use alternative registry, may be a folder or a file")
    parser.add_argument("-e", "--edit", help="Open a editor for registry file")

    parser.add_argument("time1", nargs="?", type=lambda s: datetime.datetime.strptime(
        s, '%H:%M'), help="Time of today's registry open. If second parameter is present, its time of yesterday registry end.")
    parser.add_argument("time2", nargs="?", type=lambda s: datetime.datetime.strptime(
        s, '%H:%M'), help="Time of today's registry open")

    return parser.parse_args()


def message(message):
    global args

    if args.verbose:
        print(message)


def get_current_period_file_name():
    return datetime.datetime.strftime("%Y-%m")


def try_path(path):
    if os.path.isfile(path):
        return path
    elif os.path.isdir(path):
        return path + "/" + get_current_period_file_name()
    else:
        raise Exception("'{}' is neither a file or a folder".format(path))


def get_registry_file_name():
    global args

    # TODO Continue here
    if args.registry:
        return try_path(args.registry)
    elif os.environment["REGISTRY_FILE"]:
        return try_path(os.environment["REGISTRY_FILE"])
    elif os.environment["XDG_HOME_DIR"]:
        return try_path(os.environment["XDG_HOME_DIR"] + "/." + args.prog)
    elif os.environment["HOME"] + "/.config/" + args.prog:
        return try_path(os.environment["HOME"] + "/.config/" + args.prog)
    elif os.environment["HOME"] + "/." + args.prog:
        return try_path(os.environment["HOME"] + "/." + args.prog)
    else:
        raise Exception("Unable to find a suitable registry file")


def is_last_registry_open():
    f = open(get_registry_file_name())


def main():
    global args
    args = parse_command_line()

    if args.time1:
        last_registry_open = is_last_registry_open()

        if args.time2:
            message(
                "Both parameters defined, closing last registry and opening a new one")
        else:
            message(
                "Just first parameter defined, closing last opened registry and opening a new one")
    else:
        message("Nothing defined, listing")

if __name__ == '__main__':
    main()
